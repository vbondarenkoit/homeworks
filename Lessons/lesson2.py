"""Задача 1."""

"""Вариант 1"""
country = 'Ukraine'
country1 = len(country)
example = 'world ' + country + ' has ' + str(country1) + ' letters'
print(example)

"""Вариант 2"""
example2 = 'world %s has %s letters' % (country, country1)
print(example2)

"""Вариант 3"""
example3 = 'world {} has {} letters'.format(country, country1)
print(example3)

"""Вариант 4"""
example4 = f'world {country} has {country1} letters'
print(example4)

"""Вариант 5"""
example5 = 'world {0} has {1} letters'
var = example5.format(country, country1)
print(var)

"""Вариант 6"""
example5 = 'world {country_value} has {country1_value} letters'
var1 = example5.format(country_value=country, country1_value=country1)
print(var1)


"""Задача 2. Хотів окремим файлом але так теж працює))"""

age = input('Введите возраст: ')
age_people = int(age)
if age_people < 7:
    print('Де твої батьки?')
if '7' in age:
    print('Вам сьогодні пощастить!')
elif age_people < 16:
    print('Це фільм для дорослих!')
elif age_people > 65:
    print('Покажіть пенсійне посвідчення!')
else:
    print('А білетів вже немає!')

