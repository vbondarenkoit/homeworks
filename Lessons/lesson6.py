import lesson7


@lesson7.count_time
def main():
    while True:
        input_age = input('Введіть будь ласка свій вік: ')
        print_age(input_age)
        is_repeat = input('Хочете повторити?[yes, no]: ')
        if is_repeat.lower() == 'no':
            break


'''
Dialogue functions
'''


def plural_form(number, nouns_array):
    delim = number % 10 if number > 20 else number
    if 0 < delim < 2:
        return f'{number} {nouns_array[0]}'
    elif 0 < delim < 5:
        return f'{number} {nouns_array[1]}'
    else:
        return f'{number} {nouns_array[2]}'


'''
Selecting the right kind of units of measure
'''


def has_repeat_numbers(number):
    number_array = [s for s in str(number)]
    return len(number_array) != len(set(number_array))


'''
Definition of double numbers
'''


def print_age(input_age):
    age_person = int(input_age)
    years_word = plural_form(age_person, ['рік', 'роки', 'років'])

    if age_person < 7:
        print('Тобі ж', years_word, '! Де твої батьки?')
    elif 7 < age_person < 16:
        print('Тобі лише', years_word, ',а це е фільм для дорослих!')
    elif age_person > 65:
        print('Вам', years_word, '? Покажіть пенсійне посвідчення!')
    else:
        print('Незважаючи на те, що вам', years_word, ',білетів всеодно нема!')

    if has_repeat_numbers(age_person):
        print('О, вам ', years_word, '! Який цікавий вік!')


'''
Determining the age range for performing specified actions
'''

if __name__ == '__main__':
    main()
