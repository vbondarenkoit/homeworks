import copy
lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
lst2 = copy.deepcopy(lst1)
list_only_str = []
for elem in lst2:
    if type(elem) == str:
        list_only_str.append(elem)
print(list_only_str)
