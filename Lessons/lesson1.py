first = 10
second = 30

res = first + second
print(res)

res1 = first - second
print(res1)

res2 = first * second
print(res2)

res3 = first / second
print(res3)

res4 = first // second
print(res4)

res5 = first % second
print(res5)

res6 = first ** second
print(res6)

check_function = first < second
print(check_function)

check_function = first > second
print(check_function)

check_function = first <= second
print(check_function)

check_function = first >= second
print(check_function)

check_function = first == second
print(check_function)

check_function = first != second
print(check_function)
