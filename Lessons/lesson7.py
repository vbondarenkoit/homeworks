import time


def count_time(main):
    def timing():
        start_time = time.time()
        main()
        print(time.time() - start_time)
    return timing
